import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import renderer from 'react-test-renderer'

test('renders welcome', () => {
  const { getByText } = render(<App />);
  const header1 = getByText(/Welcome/i);
  expect(header1).toBeInTheDocument();
});
