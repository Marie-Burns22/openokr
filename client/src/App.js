import React from 'react';
import './App.css';
import ObjectiveForm from './ObjectiveForm';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Welcome to Open OKR!</h1>
        <ObjectiveForm />
      </header>
    </div>
  );
}

export default App;
