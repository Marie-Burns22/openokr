class RemoveMeasureFromKeyResults < ActiveRecord::Migration[6.0]
  def change
    remove_column :key_results, :measure, :string
  end
end
