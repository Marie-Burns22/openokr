require 'rails_helper'

RSpec.describe User, type: :model do
  it { should have_many(:objectives).dependent(:destroy) }
  # it { should have_many(:key_results).through(:objectives) }

  it { should { should validate_presence_of(:name) } }
  it { should { should validate_presence_of(:email) } }
  it { should { should validate_presence_of(:password_digest) } }
end