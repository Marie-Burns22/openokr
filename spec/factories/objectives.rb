require 'faker'

FactoryBot.define do
  factory :objective do
    description { Faker::Lorem.word }
    score { Faker::Number.number(digits: 1) }
    user
  end
end