require 'rails_helper'

RSpec.describe 'Objectives API', type: :request do

  let(:user) { create(:user) }
  let!(:objectives) { create_list(:objective, 10, user_id: user.id) }
  let(:user_id) { user.id }
  let(:objective_id) { objectives.first.id }
  let(:headers) { valid_headers }

  describe 'GET /api/v1/users/:user_id/objectives' do
    before { get "/api/v1/users/#{user_id}/objectives", params: {}, headers: headers }

    it 'returns objectives' do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end
  
    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /api/v1/users/:user_id/objectives/:id' do
    before { get "/api/v1/users/#{user_id}/objectives/#{objective_id}", params: {}, headers: headers }

    context 'when the record exists' do
      it 'returns the objective' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(objective_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:objective_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Objective/)
        end
      end
    end

    describe 'POST /api/v1/users/:user_id/objectives' do
      let(:valid_attributes) do
        { description: 'Grow revenue by 20%', score: 1.0, created_by: user.id.to_s }.to_json
      end


      context 'when the request is valid' do
        before { post "/api/v1/users/#{user_id}/objectives", params: valid_attributes, headers: headers }

        it 'creates an objective' do
          expect(json['description']).to eq('Grow revenue by 20%')
          expect(json['score']).to eq(1.0)
        end

        it 'returns status code 201' do
          expect(response).to have_http_status(201)
        end
      end

      context 'when the request is invalid' do
        let(:invalid_attributes) { { description: nil }.to_json }
        before { post "/api/v1/users/#{user_id}/objectives", params: invalid_attributes, headers: headers }

        it 'returns status code 422' do
          expect(response).to have_http_status(422)
        end

        it 'returns a validation failure message' do
          expect(json['message'])
          .to match(/Validation failed: Description can't be blank/)
        end
      end
    end
  
  describe 'PUT /api/v1/users/:user_id/objectives/:id' do
    let(:valid_attributes) { { description: 'Grow revenue by 21%' }.to_json }

    context 'when record exists' do
      before { put "/api/v1/users/#{user_id}/objectives/#{objective_id}", params: valid_attributes, headers: headers }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  describe 'DELETE /api/v1/users/:user_id/objectives/:id' do
    before { delete "/api/v1/users/#{user_id}/objectives/#{objective_id}", params: {}, headers: headers }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end

end