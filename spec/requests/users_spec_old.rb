require 'rails_helper'

RSpec.describe 'Users API', type: :request do

  let!(:users) { create_list(:user, 10) }
  let(:user_id) { users.first.id }

  describe 'GET /api/v1/users' do
    before { get '/api/v1/users' }

    it 'returns users' do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  
  end

  describe 'POST /api/v1/users' do
      let(:valid_attributes) {{ name: 'Matt Johnson', email: 'matt@gmail.com', password: 'oiuwejrfd0s8234' }}

      context 'when the request is valid' do
        before { post "/api/v1/users", params: valid_attributes }

        it 'returns status code 201' do
          expect(response).to have_http_status(201)
        end
      end

      context 'when the request is invalid' do
        before { post "/api/v1/users", params: { name: 'Matt Johnson', password: 'oiuwejrfd0s8234' } }

        it 'returns status code 422' do
          expect(response).to have_http_status(422)
        end

        it 'returns a validation failure message' do
          expect(response.body)
          .to match(/Validation failed: Email can't be blank/)
        end
      end

    describe 'PUT /api/v1/users/:id' do
      let(:valid_attributes) { { name: 'Matt Johannsson' } }

      context 'when record exists' do
        before { put "/api/v1/users/#{user_id}", params: valid_attributes }

        it 'updates the record' do
          expect(response.body).to be_empty
        end

        it 'returns status code 204' do
          expect(response).to have_http_status(204)
        end
      end
    end

    describe 'DELETE /api/v1/users/:id' do
      before { delete "/api/v1/users/#{user_id}" }

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end

    end
  end