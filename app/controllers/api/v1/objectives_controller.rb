module Api
  module V1
    class ObjectivesController < ApplicationController
       before_action :set_objective, only: [:show, :update, :destroy]

      def index
        @objectives = current_user.objectives
        json_response(@objectives)
      end

      def create
        @objective = current_user.objectives.create!(objectives_params)
        json_response(@objective, :created)
      end

      def show
        json_response(@objective)
      end

      def update
        @objective.update(objectives_params)
        head :no_content
      end

      def destroy
        @objective.destroy
        head :no_content
      end

    private

      def objectives_params
        params.permit(:description, :score, :user_id, :objective_id)
      end

      def set_objective
        @objective = Objective.find(params[:id])
      end

    end
  end
end