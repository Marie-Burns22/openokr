module Api
  module V1
    class KeyResultsController < ApplicationController
      before_action :set_objective
      before_action :set_key_result, only: [:show, :update, :destroy]

      def index
        json_response(@objective.key_results)
      end

      def create
        @objective.key_results.create!(key_results_params)
        json_response(@key_result, :created)
      end

      def show
        json_response(@key_result)
      end

      def update
        @key_result.update(key_results_params)
        head :no_content
      end

      def destroy
        @key_result.destroy
        head :no_content
      end

    private

      def key_results_params
        params.permit(:description, :due_date, :user_id, :objective_id)
      end

      def set_objective
        @objective = Objective.find(params[:objective_id])
      end

      def set_key_result
        @key_result = @objective.key_results.find_by!(id: params[:id]) if @objective
      end

    end
  end
end