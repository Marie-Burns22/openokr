class Objective < ApplicationRecord
  belongs_to :user
  has_many :key_results, dependent: :destroy

  validates :description, presence: :true
  validates :score, presence: :true
end
