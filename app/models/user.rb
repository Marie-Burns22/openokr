class User < ApplicationRecord
  has_secure_password

  has_many :objectives, dependent: :destroy
  has_many :key_results, through: :objective

  validates :name, presence: true
  validates :email, presence: true
  validates :password, presence: :true

  validates_associated :objectives
end