Rails.application.routes.draw do
  
  post 'auth/login', to: 'authentication#authenticate'
  post 'signup', to: 'user#create'

  namespace :api do
    namespace :v1 do
      resources :users do
        resources :objectives do
          resources :key_results
        end
      end
    end
  end

end
